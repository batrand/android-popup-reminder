package batrand.android.popupreminder.models;

import java.util.UUID;

import batrand.android.popupreminder.models.reminder.Reminder;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by batra on 2017-05-24.
 */

public class Notification extends RealmObject {

    // Required for Realm
    public Notification() {}
    public Notification(Reminder reminder) {
        id = UUID.randomUUID().toString();
        timestamp = System.currentTimeMillis();

        reminderId = reminder.id();
        reminderTitle = reminder.getTitle();
        reminderDescription = reminder.getDescription();

        isSeen = false;
    }

    @PrimaryKey private String id;
    public String id() { return id; }

    private long timestamp;
    public long timestamp() { return timestamp; }

    private String reminderId;
    public String reminderId() { return reminderId; }

    private String reminderTitle;
    public String reminderTitle() { return reminderTitle; }

    private String reminderDescription;
    public String reminderDescription() { return reminderDescription; }

    private boolean isSeen;
    public boolean isSeen() { return isSeen; }
    public void markSeen() { isSeen = true; }
}
