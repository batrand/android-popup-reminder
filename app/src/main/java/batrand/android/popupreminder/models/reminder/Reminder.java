package batrand.android.popupreminder.models.reminder;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

import batrand.android.popupreminder.services.reminder.IReminderService;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by batra on 2017-05-09.
 */

public class Reminder extends RealmObject {

    // Required for Realm
    public Reminder() {
        id = UUID.randomUUID().toString();
        timestamp = System.currentTimeMillis();
    }

    @PrimaryKey private String id;
    public String id() { return id; }

    private long timestamp;
    public long timestamp() { return timestamp; }

    private String title;
    public void setTitle(String title) { this.title = title; }
    public String getTitle() { return title; }

    private String description;
    public void setDescription(String description) { this.description = description; }
    public String getDescription() { return description; }

    public boolean isValidReminder(IReminderService.IConfigs reminderConfigs) {
        boolean isValid = true;
        if(reminderType == -1) isValid = false;
        if(getReminderType() == ReminderType.TRIGGER)
            if(triggerTime == -1 || triggerTime < System.currentTimeMillis()) isValid = false;
        else if(getReminderType() == ReminderType.INTERVAL)
            if(interval == -1 && !reminderConfigs.isValidInterval(getIntervalMillis(interval, getIntervalType())))
                isValid = false;
        return isValid;
    }

    private int reminderType = -1;
    private void setReminderType(ReminderType type) { reminderType = type.toInt(); }
    public ReminderType getReminderType() { return ReminderType.from(reminderType); }

    private long triggerTime = -1;
    public long getTriggerTime() { return triggerTime; }
    public void setAsTrigger(long triggerTime) {
        setReminderType(ReminderType.TRIGGER);
        lastTriggerTime = -1; // reset on edit
        this.triggerTime = triggerTime;
    }

    private int interval = -1;
    public int getInterval() { return interval; }
    private int intervalType = -1;
    private long intervalStart;
    public long getIntervalStart() { return intervalStart; }
    public ReminderIntervalType getIntervalType() { return ReminderIntervalType.from(intervalType); }
    public void setAsInterval(ReminderIntervalType intervalType, int interval, long intervalStart) {
        setReminderType(ReminderType.INTERVAL);
        this.intervalType = intervalType.toInt();
        this.interval = interval;
        this.intervalStart = intervalStart;
    }

    private long lastTriggerTime = -1;
    public void trigger() { lastTriggerTime = System.currentTimeMillis(); }
    public boolean isTriggered() { return lastTriggerTime != -1; }
    public long getLastTriggerTime() { return lastTriggerTime; }

    public long getNextAlarmMillis(IReminderService.IConfigs configs) {
        //Trigger
        if(getReminderType() == ReminderType.TRIGGER) return triggerTime;

        // Interval
        else {
            // Interval not triggered, next time is startTime.
            if(!isTriggered()) return intervalStart;

            // Interval already triggered. Next trigger is a number of interval times
            // since intervalStart that is greater than current system time.
            else {
                long intervalMillis = getIntervalMillis(getInterval(), getIntervalType());
                long nextMillis = intervalStart + intervalMillis;
                long currentMillis = System.currentTimeMillis();
                // Add interval time in millis until it's greater than current time.
                while(currentMillis > nextMillis)
                    nextMillis += intervalMillis;
                return nextMillis;
            }
        }
    }

    public static long getIntervalMillis(int interval, ReminderIntervalType intervalType) {
        if(intervalType == ReminderIntervalType.MINUTE)
            return TimeUnit.MINUTES.toMillis(interval);
        else if(intervalType == ReminderIntervalType.HOUR)
            return TimeUnit.HOURS.toMillis(interval);
        else if(intervalType == ReminderIntervalType.DAY)
            return TimeUnit.DAYS.toMillis(interval);
        else if(intervalType == ReminderIntervalType.WEEK)
            return TimeUnit.DAYS.toMillis(7) * interval;
        else if(intervalType == ReminderIntervalType.MONTH)
            return TimeUnit.DAYS.toMillis(30) * interval;
            // Year
        else return TimeUnit.DAYS.toMillis(365) * interval; // 3.15576e+10
    }

    /**
     * @return true if this is a trigger that is not triggered and is late
     */
    public boolean isLateTrigger() {
        return getReminderType() == ReminderType.TRIGGER
                && !isTriggered()
                && System.currentTimeMillis() > triggerTime;
    }
}
