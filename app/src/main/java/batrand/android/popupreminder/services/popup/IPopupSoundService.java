package batrand.android.popupreminder.services.popup;

import android.content.Context;

/**
 * Created by batra on 2017-07-05.
 */

public interface IPopupSoundService {
    void playSound(Context context);
}
