package batrand.android.popupreminder.services.reminder;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import batrand.android.popupreminder.R;
import batrand.android.popupreminder.models.Notification;
import batrand.android.popupreminder.models.reminder.Reminder;
import batrand.android.popupreminder.models.reminder.ReminderType;
import batrand.android.popupreminder.services.alarm.IAlarmService;
import batrand.android.popupreminder.services.formatter.IFormatService;
import batrand.android.popupreminder.services.log.ILog;
import batrand.android.popupreminder.services.popup.IPopupService;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by batra on 2017-05-09.
 */
@Singleton
public class ReminderService implements IReminderService {

    //region Configs
    @Override
    public IConfigs getConfigs() {
        return new Configs();
    }

    public class Configs implements IReminderService.IConfigs {
        @Override public String getChangeAvailableBroadcastString() {
            return "reminder-service-change-available";
        }
        @Override public long getMinimumIntervalMillis() { return 600000; } // 10 minutes
        @Override public int getStringRIdOfReadableMinimumInterval() { return R.string.min_interval; }
        @Override public long getMaximumIntervalMillis() { return 31557600000L; } // 1 year
        @Override public int getStringRIdOfReadableMaximumInterval() { return R.string.max_interval; }
        @Override public boolean isValidInterval(long interval) {
            return getMinimumIntervalMillis() <= interval && interval <= getMaximumIntervalMillis();
        }
    }
    //endregion

    //region Construction, injection
    private IAlarmService mAlarmService;
    private IPopupService mPopupService;
    private ILog mLog;
    private IFormatService mFormatter;
    @Inject public ReminderService(IAlarmService alarm, IPopupService popup, ILog log, IFormatService format) {
        mAlarmService = alarm;
        mPopupService = popup;
        mLog = log;
        mFormatter = format;
    }
    //endregion

    //region Reminder registration & retrieval
    @Override
    public boolean registerReminder(Context context, final Reminder reminder) {
        if(!reminder.isValidReminder(getConfigs())) return false;

        // Persist reminder
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm tRealm) {
                tRealm.copyToRealmOrUpdate(reminder);
            }
        });
        realm.close();

        // Set alarm
        long nextAlarmMillis = reminder.getNextAlarmMillis(getConfigs());
        mAlarmService.setAlarm(context, reminder.id(), nextAlarmMillis, null);
        mLog.log("Alarm for reminder " + reminder.getTitle() + " with ID " + reminder.id()
                + " set for " + mFormatter.formatDateTimeCompact(context, nextAlarmMillis));

        broadcastChange(context);

        return true;
    }

    @Override
    public void unregisterReminder(Context context, final String reminderId) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm tRealm) {
                Reminder reminder = tRealm.where(Reminder.class).equalTo("id",reminderId).findFirst();
                if(reminder != null) reminder.deleteFromRealm();
            }
        });
        realm.close();

        // Cancel alarm
        mAlarmService.cancelAlarm(context, reminderId);
        mLog.log("Canceled alarm for " + reminderId);

        broadcastChange(context);
    }

    @Override
    public Reminder getReminder(String reminderId) {
        Realm realm = Realm.getDefaultInstance();
        Reminder managedReminder = realm.where(Reminder.class).equalTo("id",reminderId).findFirst();

        Reminder reminder = null;
        if(managedReminder != null) reminder = realm.copyFromRealm(managedReminder);

        realm.close();
        return reminder;
    }

    @Override
    public List<Reminder> getAllReminders() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Reminder> results = realm.where(Reminder.class).findAllSorted("timestamp", Sort.DESCENDING);
        List<Reminder> reminders = realm.copyFromRealm(results);
        realm.close();
        return reminders;
    }
    //endregion

    @Override
    public void onReminderTriggered(final Context fromContext, final String triggeredReminderId) {
        final Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm tRealm) {
                Reminder reminder = tRealm.where(Reminder.class).equalTo("id",triggeredReminderId).findFirst();
                if(reminder==null) {
                    mLog.log(tRealm, "onReminderTriggered called with invalid ID - no reminder with such ID exists!");
                    return;
                }

                reminder.trigger();
                tRealm.copyToRealmOrUpdate(reminder);
                Notification notification = new Notification(reminder);
                tRealm.copyToRealm(notification);
                mPopupService.popup(fromContext, notification.id());

                broadcastChange(fromContext);

                if(reminder.getReminderType() == ReminderType.INTERVAL) {
                    long nextAlarmMillis = reminder.getNextAlarmMillis(getConfigs());
                    mAlarmService.setAlarm(fromContext, reminder.id(), nextAlarmMillis, null);
                    mLog.log(tRealm,
                            "Next alarm for interval " + reminder.getTitle()
                                    + " set for "
                                    + mFormatter.formatDateTimeCompact(fromContext, nextAlarmMillis));
                }
            }
        });
        realm.close();
    }

    @Override
    public Notification getNotification(String notificationId) {
        Realm realm = Realm.getDefaultInstance();
        Notification managedNotification = realm.where(Notification.class).equalTo("id",notificationId).findFirst();

        Notification notification = null;
        if(managedNotification != null) notification = realm.copyFromRealm(managedNotification);

        realm.close();
        return notification;
    }

    @Override
    public List<Notification> getAllNotifications() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Notification> results = realm.where(Notification.class).findAllSorted("timestamp", Sort.DESCENDING);
        List<Notification> notifications = realm.copyFromRealm(results);
        realm.close();
        return notifications;
    }

    @Override
    public int getUnseenNotificationCount() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Notification> results = realm.where(Notification.class).equalTo("isSeen",false).findAll();
        return results.size();
    }

    @Override
    public void markNotificationSeen(Context context, String notificationId) {
        final Notification notification = getNotification(notificationId);
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm tRealm) {
                notification.markSeen();
                tRealm.copyToRealmOrUpdate(notification);
            }
        });
        realm.close();

        broadcastChange(context);
    }

    @Override
    public void markAllNotificationsSeen(Context context) {
        List<Notification> allNotifs = getAllNotifications();
        for(Notification notif : allNotifs) {
            if(!notif.isSeen()) markNotificationSeen(context, notif.id());
        }
    }

    @Override
    public void deleteNotification(Context context, final String notificationId) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm tRealm) {
                Notification notification = tRealm.where(Notification.class).equalTo("id",notificationId).findFirst();
                if(notification != null) notification.deleteFromRealm();
            }
        });
        realm.close();

        broadcastChange(context);
    }

    @Override
    public void resetAlarms(Context context) {
        mLog.log("Resetting alarms...");

        // Restore trigger reminders
        for(Reminder trigger : getUntriggeredTriggerReminders()) {
            resetAlarmForReminder(context, trigger);
        }

        // Restore interval reminders
        for(Reminder interval : getIntervalReminders()) {
            resetAlarmForReminder(context, interval);
        }

        mLog.log("Alarms reset.");
    }

    private void resetAlarmForReminder(Context context, Reminder reminder) {
        // Cancel any alarm
        if(mAlarmService.isAlarmSet(context, reminder.id()))
            mAlarmService.cancelAlarm(context, reminder.id());

        // Register again
        boolean successful = registerReminder(context, reminder);
        if(successful) mLog.log("Reset successful for reminder " + reminder.getTitle());
        else {
            mLog.log("Reset unsuccessful for reminder " + reminder.getTitle());

            // Is it a trigger that is late? trigger it immediately.
            if(reminder.isLateTrigger()) {
                onReminderTriggered(context, reminder.id());
                mLog.log("Triggered late reminder " + reminder.getTitle());
            }
        }
    }

    private List<Reminder> getUntriggeredTriggerReminders() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Reminder> results = realm.where(Reminder.class)
                .equalTo("reminderType", ReminderType.TRIGGER.toInt())
                .equalTo("lastTriggerTime", -1)
                .findAll();
        List<Reminder> reminders = realm.copyFromRealm(results);
        realm.close();
        return reminders;
    }

    private List<Reminder> getIntervalReminders() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Reminder> results = realm.where(Reminder.class)
                .equalTo("reminderType", ReminderType.INTERVAL.toInt())
                .findAll();
        List<Reminder> reminders = realm.copyFromRealm(results);
        realm.close();
        return reminders;
    }

    private void broadcastChange(Context fromContext) {
        LocalBroadcastManager.getInstance(fromContext)
                .sendBroadcast(new Intent(getConfigs().getChangeAvailableBroadcastString()));
    }
}
