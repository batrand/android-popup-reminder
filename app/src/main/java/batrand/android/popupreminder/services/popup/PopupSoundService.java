package batrand.android.popupreminder.services.popup;

import android.content.Context;
import android.media.MediaPlayer;

import javax.inject.Inject;

import batrand.android.popupreminder.services.settings.ISettingsService;

import static batrand.android.popupreminder.services.injection.SingletonsInjector.getInjector;

/**
 * Created by batra on 2017-07-05.
 */

public class PopupSoundService implements IPopupSoundService {

    @Inject ISettingsService mSettings;

    public PopupSoundService() {
        getInjector().inject(this);
    }

    @Override
    public void playSound(Context context) {
        int soundRId = mSettings.getPopupSoundRawRId(context);

        final MediaPlayer mediaPlayer = MediaPlayer.create(context, soundRId);
        float volume = mSettings.getSoundVolume(context);
        mediaPlayer.setVolume(volume, volume);
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mediaPlayer.reset();
                mediaPlayer.release();
            }
        });
        mediaPlayer.start();
    }
}
