package batrand.android.popupreminder.views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;
import android.view.View;

import javax.inject.Inject;

import batrand.android.popupreminder.R;
import batrand.android.popupreminder.services.popup.IPopupService;
import batrand.android.popupreminder.services.reminder.IReminderService;
import batrand.android.popupreminder.services.settings.ISettingsService;
import batrand.android.popupreminder.views.bases.BaseActivity;
import batrand.android.popupreminder.views.bases.BaseFragment;
import batrand.android.popupreminder.views.callbacks.IMainCabActivity;

import static batrand.android.popupreminder.services.injection.SingletonsInjector.getInjector;

public class MainActivity extends BaseActivity {

    @Inject IPopupService mPopupService;
    @Inject IReminderService mReminderService;
    @Inject ISettingsService mSettings;

    private BottomBarHandler mBottomBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getInjector().inject(this);

        // Setup custom action bar
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.view_titlebar);

        // Set up fragments
        BottomBarButtonHandler remindersHandler = new BottomBarButtonHandler(
                findViewById(R.id.main_bottombutton_reminders),
                RemindersFragment.newInstance(mCabController)
        );
        BottomBarButtonHandler notificationsHandler = new BottomBarButtonHandler(
                findViewById(R.id.main_bottombutton_notifications),
                NotificationsFragment.newInstance(mCabController)
        );
        mBottomBar = new BottomBarHandler(
                new BottomBarButtonHandler[] { remindersHandler, notificationsHandler },
                findViewById(R.id.main_bottombutton_menu),
                findViewById(R.id.main_bottombutton_menu_anchor),
                this, R.menu.main_bottom_menu,
                getSupportFragmentManager(), R.id.main_container
        );

        // Open first screen
        mBottomBar.select(0);

        // Handle show internet permission rationale
        if(!mSettings.getHasSeenInternetPermissionRationale(this)) {
            startActivity(new Intent(this, InternetRationaleActivity.class));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!mPopupService.hasPermission(this)) {
            mPopupService.processPermission(this);
            finish();
        }
    }

    //region CAB
    private int mStatusBarColor = R.color.colorPrimaryDark;
    private int mStatusBarColorActionMode = R.color.colorAccent;
    private IMainCabActivity mCabController = new IMainCabActivity() {
        @SuppressLint("RestrictedApi")
        @Override
        public void startCab(ActionMode.Callback callback) {
            getSupportActionBar().startActionMode(callback);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().setStatusBarColor(getResources().getColor(mStatusBarColorActionMode));
            }
        }

        @Override
        public void onCabStopped() {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().setStatusBarColor(getResources().getColor(mStatusBarColor));
            }
        }
    };
    //endregion

    /**
     * Abstraction layer for bottom bar buttons
     */
    private class BottomBarButtonHandler {
        private View mButton;
        View getButton() { return mButton; }

        private BaseFragment mFragment;
        BaseFragment getFragment() { return mFragment; }

        BottomBarButtonHandler(View button, BaseFragment fragment) {
            mButton = button;
            mFragment = fragment;
        }

        void select(boolean isSelected) {
            mButton.setActivated(isSelected);
        }
    }

    private class BottomBarHandler {
        private BottomBarButtonHandler[] mButtons;
        private FragmentManager mManager;
        private int mContainerId;
        private int mCurrentIndex = -1;

        BottomBarHandler(BottomBarButtonHandler[] buttons,
                         View menuButton, View menuAnchor, Context contextWithStyle, int menuRId,
                         FragmentManager manager, int containerId) {
            mButtons = buttons;
            mManager = manager;
            mContainerId = containerId;

            setupClickListeners();
            setupMenu(menuButton, menuAnchor, contextWithStyle, menuRId);
        }

        void select(int buttonIndex) {
            if(mCurrentIndex == buttonIndex) return;

            for(int i = 0; i < mButtons.length; i++) {
                if(i == buttonIndex) mButtons[i].select(true);
                else mButtons[i].select(false);
            }

            mManager.beginTransaction()
                    .replace(mContainerId, mButtons[buttonIndex].getFragment())
                    .commit();

            mCurrentIndex = buttonIndex;
        }

        private void setupClickListeners() {
            for(int i = 0; i < mButtons.length; i++) {
                final int index = i;
                mButtons[i].getButton().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        select(index);
                    }
                });
            }
        }

        private View mMenuButton;
        private View mMenuButtonAnchor;
        private PopupMenu mMenu;

        private void setupMenu(View button, View anchor, final Context context, int menuRId) {
            mMenuButton = button;
            mMenuButtonAnchor = anchor;
            mMenuButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mMenu.show();
                    mMenuButton.setActivated(true);
                }
            });

            mMenu = new PopupMenu(context, mMenuButtonAnchor);
            mMenu.inflate(menuRId);
            mMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    switch(item.getItemId()) {
                        case R.id.bottommenuitem_settings:
                            startActivity(new Intent(MainActivity.this, SettingsActivity.class));
                            return true;
                        case R.id.bottommenuitem_about:
                            startActivity(new Intent(MainActivity.this, AboutActivity.class));
                            return true;
                        default: return false;
                    }
                }
            });
            mMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
                @Override
                public void onDismiss(PopupMenu menu) {
                    mMenuButton.setActivated(false);
                }
            });
        }
    }

}
