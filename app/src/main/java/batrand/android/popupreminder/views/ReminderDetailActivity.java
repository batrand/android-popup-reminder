package batrand.android.popupreminder.views;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatSpinner;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.inject.Inject;

import batrand.android.popupreminder.R;
import batrand.android.popupreminder.models.reminder.Reminder;
import batrand.android.popupreminder.models.reminder.ReminderIntervalType;
import batrand.android.popupreminder.models.reminder.ReminderType;
import batrand.android.popupreminder.services.formatter.IFormatService;
import batrand.android.popupreminder.services.reminder.IReminderService;
import batrand.android.popupreminder.services.settings.ISettingsService;
import batrand.android.popupreminder.views.bases.BaseActivity;

import static batrand.android.popupreminder.services.injection.SingletonsInjector.getInjector;

/**
 * For adding or editing reminders
 */
public class ReminderDetailActivity extends BaseActivity {

    public static final int ADD_REQUEST = 25487;
    public static final int EDIT_REQUEST = 35847;
    /**
     * Use this as key for Edit Mode. Its value should be the ID
     * of the reminder to be edited.
     */
    public static final String EDIT_MODE_KEY = "EDIT_MODE";

    @Inject IFormatService mFormatter;
    @Inject IReminderService mReminderService;
    @Inject ISettingsService mSettings;

    private CoordinatorLayout mCoordinator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder_detail);
        getInjector().inject(this);

        mCoordinator = (CoordinatorLayout) findViewById(R.id.addreminder_coordinator);
        mDateField = (TextView) findViewById(R.id.triggerDateField);
        mTimeField = (TextView) findViewById(R.id.triggerTimeField);
        mTitleField = (EditText) findViewById(R.id.reminderTitleField);
        mDescriptionField = (EditText) findViewById(R.id.reminderDescriptionField);
        mIntervalField = (EditText) findViewById(R.id.intervalField);

        mIsIntervalCheckbox = (AppCompatCheckBox) findViewById(R.id.reminder_is_interval_checkbox);

        findViewById(R.id.addreminder_donebutton).setOnClickListener(mAddButtonListener);
        findViewById(R.id.triggerDateFieldListener).setOnClickListener(mDateFieldListener);
        findViewById(R.id.triggerTimeFieldListener).setOnClickListener(mTimeFieldListener);

        mIntervalTypeStrings = getResources().getStringArray(R.array.interval_types);

        mIntervalTypeSpinner = (AppCompatSpinner) findViewById(R.id.intervalTypeSpinner);
        ArrayAdapter<CharSequence> intervalTypeAdapter = ArrayAdapter.createFromResource(this, R.array.interval_types, android.R.layout.simple_spinner_item);
        intervalTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mIntervalTypeSpinner.setAdapter(intervalTypeAdapter);
        mIntervalTypeSpinner.setOnItemSelectedListener(mIntervalTypeListener);

        prefillFields();

        if(getIntent().getExtras()!=null) {
            String editReminderId = getIntent().getExtras().getString(EDIT_MODE_KEY);
            if(editReminderId == null) return;

            // Is in edit mode

            mEditReminder = mReminderService.getReminder(editReminderId);
            setTitle(R.string.title_edit_reminder);

            populateEditFields();
        }
    }

    //region Edit mode
    private Reminder mEditReminder;

    /**
     * Populate fields with information from the reminder being edited
     */
    private void populateEditFields() {
        mTitleField.setText(mEditReminder.getTitle());
        mDescriptionField.setText(mEditReminder.getDescription());

        // Populate time fields
        long triggerTime;
        if(mEditReminder.getReminderType() == ReminderType.TRIGGER)
            triggerTime = mEditReminder.getTriggerTime();
        else triggerTime = mEditReminder.getIntervalStart();
        setTimeFields(triggerTime);

        if(mEditReminder.getReminderType() == ReminderType.INTERVAL) {
            mIsIntervalCheckbox.setChecked(true);
            mIntervalField.setText(Integer.toString(mEditReminder.getInterval()));
            mIntervalTypeSpinner.setSelection(mEditReminder.getIntervalType().toInt());
        }
    }
    //endregion

    //region Fields
    private View.OnClickListener mAddButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // Validation
            if(isTitleEmpty()) {
                showSnackbarWithNullAction(mCoordinator, getString(R.string.error_title_empty), getString(R.string.ok));
                return;
            }
            if(!mDateSet || !mTimeSet) {
                showSnackbarWithNullAction(mCoordinator, getString(R.string.error_trigger_time_not_set), getString(R.string.ok));
                return;
            }
            if(!isTriggerTimeInFuture()) {
                showSnackbarWithNullAction(mCoordinator, getString(R.string.error_trigger_time_past), getString(R.string.ok));
                return;
            }
            if(isInterval()) {
                if(interval() == 0) {
                    showSnackbarWithNullAction(mCoordinator, getString(R.string.error_interval_empty), getString(R.string.ok));
                    return;
                }
                if(!mReminderService.getConfigs().isValidInterval(Reminder.getIntervalMillis(interval(), intervalType()))) {
                    showSnackbarWithNullAction(mCoordinator,
                            getString(R.string.error_interval_out_of_range,
                                    getString(mReminderService.getConfigs().getStringRIdOfReadableMinimumInterval()),
                                    getString(mReminderService.getConfigs().getStringRIdOfReadableMaximumInterval())),
                            getString(R.string.ok));
                    return;
                }
            }

            // Construct reminder
            Reminder reminder = new Reminder();
            if(mEditReminder != null) reminder = mEditReminder;
            reminder.setTitle(title());
            if(!isDescriptionEmpty())
                reminder.setDescription(description());
            if(isInterval()) reminder.setAsInterval(intervalType(), interval(), triggerTime());
            else reminder.setAsTrigger(triggerTime());

            if(!reminder.isValidReminder(mReminderService.getConfigs())) {
                // All validation done above. Any errors should be informed to the user above.
                // This only prevents adding invalid Reminders to the database
                return;
            }

            mReminderService.registerReminder(ReminderDetailActivity.this, reminder);
            setResult(RESULT_OK);
            finish();
        }
    };

    private EditText mTitleField;
    private EditText mDescriptionField;
    private EditText mIntervalField;
    private String title() { return mTitleField.getText().toString(); }
    private String description() { return mDescriptionField.getText().toString(); }
    private int interval() {
        if(mIntervalField.getText().toString().length()<1) return 0;
        else return Integer.parseInt(mIntervalField.getText().toString());
    }
    private ReminderIntervalType intervalType() {
        return ReminderIntervalType.from(mSelectedType);
    }
    private boolean isTitleEmpty() { return title().length() == 0; }
    private boolean isDescriptionEmpty() { return description().length() == 0; }
    private AppCompatCheckBox mIsIntervalCheckbox;
    private boolean isInterval() { return mIsIntervalCheckbox.isChecked(); }

    /**
     * Set Time and Date fields to this UNIX timestamp
     */
    private void setTimeFields(long millis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(millis);
        mTimeSelection = calendar;
        mDateSelection = calendar;
        updateTimeField();
        updateDateField();
    }

    private void prefillFields() {
        if(mSettings.shouldPrefillWithCurrentTime(this)) {
            setTimeFields(System.currentTimeMillis());
        }

        mIntervalField.setText(Integer.toString(mSettings.getDefaultInterval(this)));
        mIntervalTypeSpinner.setSelection(mSettings.getDefaultIntervalType(this));
    }
    //endregion

    //region Date Time picking
    private TextView mDateField;
    private boolean mDateSet = false;
    private TextView mTimeField;
    private boolean mTimeSet = false;
    private Calendar mDateSelection;
    private Calendar mTimeSelection;

    private View.OnClickListener mDateFieldListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Calendar now = Calendar.getInstance();
            if(mDateSet) now.setTime(mDateSelection.getTime());

            DatePickerDialog.newInstance(mDateSetListener,
                    now.get(Calendar.YEAR),
                    now.get(Calendar.MONTH),
                    now.get(Calendar.DAY_OF_MONTH)
            ).show(getFragmentManager(), "DatePicker");
        }
    };

    private View.OnClickListener mTimeFieldListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Calendar now = Calendar.getInstance();
            if(mTimeSet) now.setTime(mTimeSelection.getTime());

            TimePickerDialog.newInstance(mTimeSetListener,
                    now.get(Calendar.HOUR_OF_DAY),
                    now.get(Calendar.MINUTE),
                    now.get(Calendar.SECOND),
                    shouldUse24h()
            ).show(getFragmentManager(), "TimePicker");
        }
    };

    private TimePickerDialog.OnTimeSetListener mTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
            mTimeSelection = Calendar.getInstance();
            mTimeSelection.setTime(new GregorianCalendar(0,0,0, hourOfDay, minute, 0).getTime());
            updateTimeField();
        }
    };

    private void updateTimeField() {
        mTimeSet = true;
        int hourOfDay = mTimeSelection.get(Calendar.HOUR_OF_DAY);
        int minute = mTimeSelection.get(Calendar.MINUTE);

        if(shouldUse24h()) mTimeField.setText(mFormatter.formatTime24(hourOfDay, minute));
        else mTimeField.setText(mFormatter.formatTime12(hourOfDay, minute));

    }

    private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
            mDateSelection = Calendar.getInstance();
            mDateSelection.setTime(new GregorianCalendar(year, monthOfYear, dayOfMonth, 0,0,0).getTime());
            updateDateField();
        }
    };

    private void updateDateField() {
        mDateSet = true;
        int year = mDateSelection.get(Calendar.YEAR);
        int monthOfYear = mDateSelection.get(Calendar.MONTH);
        int dayOfMonth = mDateSelection.get(Calendar.DAY_OF_MONTH);
        mDateField.setText(mFormatter.formatDate(year, monthOfYear, dayOfMonth));
    }

    private long triggerTime() {
        Calendar triggerTime = Calendar.getInstance();
        triggerTime.set(
                mDateSelection.get(Calendar.YEAR),
                mDateSelection.get(Calendar.MONTH),
                mDateSelection.get(Calendar.DAY_OF_MONTH),
                mTimeSelection.get(Calendar.HOUR_OF_DAY),
                mTimeSelection.get(Calendar.MINUTE),
                mTimeSelection.get(Calendar.SECOND)
        );
        return triggerTime.getTimeInMillis();
    }

    private boolean isTriggerTimeInFuture() {
        return triggerTime() > System.currentTimeMillis();
    }
    //endregion

    //region Interval types
    private AppCompatSpinner mIntervalTypeSpinner;
    private String[] mIntervalTypeStrings;
    private String mSelectedType;
    private AdapterView.OnItemSelectedListener mIntervalTypeListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            mSelectedType = mIntervalTypeStrings[position].toUpperCase();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {}
    };
    //endregion

    private boolean shouldUse24h() {
        return mSettings.getShouldUse24hTime(this);
    }
}
