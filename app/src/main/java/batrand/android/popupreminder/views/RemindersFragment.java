package batrand.android.popupreminder.views;

import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import batrand.android.popupreminder.R;
import batrand.android.popupreminder.services.reminder.IReminderService;
import batrand.android.popupreminder.views.adapters.ReloadDoneCallback;
import batrand.android.popupreminder.views.adapters.RemindersAdapter;
import batrand.android.popupreminder.views.bases.BaseFragment;
import batrand.android.popupreminder.views.callbacks.IMainCabActivity;
import batrand.android.popupreminder.views.callbacks.IOnRecyclerItemLongClick;
import batrand.android.popupreminder.views.components.EmptyableRecyclerView;

import static batrand.android.popupreminder.services.injection.SingletonsInjector.getInjector;

public class RemindersFragment extends BaseFragment {

    @Override protected int getLayoutId() { return R.layout.fragment_reminders; }
    public RemindersFragment() {} // Required empty public constructor
    public static RemindersFragment newInstance(IMainCabActivity cabHost) {
        RemindersFragment fragment = new RemindersFragment();
        fragment.setCabHost(cabHost);
        return fragment;
    }

    @Inject IReminderService mReminderService;

    private CoordinatorLayout mCoordinator;
    private FloatingActionButton mAddButton;

    private EmptyableRecyclerView mRecycler;
    private RemindersAdapter mAdapter;
    private SwipeRefreshLayout mRefresh;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutId(), container, false);
        getInjector().inject(this);

        mCoordinator = view.findViewById(R.id.reminders_coordinator);
        mAddButton = view.findViewById(R.id.reminders_add_button);
        mAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getActivity(), ReminderDetailActivity.class), ReminderDetailActivity.ADD_REQUEST);
            }
        });

        mRecycler = view.findViewById(R.id.reminders_recycler);
        mAdapter = new RemindersAdapter(this, mItemLongClickHandler);
        mRecycler.setAdapter(mAdapter);
        mRecycler.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        mRecycler.setEmptyView(view.findViewById(R.id.reminders_recycler_empty));

        mRefresh = view.findViewById(R.id.reminders_recycler_swipe);
        mRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mAdapter.reload(new ReloadDoneCallback() {
                    @Override
                    public void onReloadDone() {
                        mRefresh.setRefreshing(false);
                    }
                });
            }
        });

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == ReminderDetailActivity.ADD_REQUEST && resultCode == Activity.RESULT_OK) {
            mAdapter.reload(null);
            showSnackbar(mCoordinator, getString(R.string.reminder_added));
        }
        if(requestCode == ReminderDetailActivity.EDIT_REQUEST && resultCode == Activity.RESULT_OK) {
            mAdapter.reload(null);
            showSnackbar(mCoordinator, getString(R.string.reminder_edited));
        }
    }

    private BroadcastReceiver mReloadReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(!intent.getAction().equals(mReminderService.getConfigs().getChangeAvailableBroadcastString()))
                return;
            mAdapter.reload(null);
        }
    };

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mReloadReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getActivity())
                .registerReceiver(mReloadReceiver,
                        new IntentFilter(mReminderService.getConfigs().getChangeAvailableBroadcastString())
                );
        checkIfNotificationIsBlocked();
    }

    private IOnRecyclerItemLongClick mItemLongClickHandler = new IOnRecyclerItemLongClick() {
        @Override
        public void onItemLongClick(int itemPosition) {
            getCabHost().startCab(mCabCallback);

            // Start CAB visuals in adapter
            mAdapter.setCabMode(mRecycler, true);
        }
    };

    private ActionMode.Callback mCabCallback = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.reminders_menu, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) { return false; }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch(item.getItemId()) {
                case R.id.reminders_menu_selectall:
                    mAdapter.selectAll(mRecycler);
                    return true;
                case R.id.reminders_menu_delete:
                    int selectionCount = mAdapter.selectionCount(mRecycler);
                    if(selectionCount < 1) return true;

                    mAdapter.deleteSelected(mRecycler);
                    mAdapter.reload(null);
                    mode.finish();

                    if(selectionCount > 1) showSnackbar(mCoordinator, getString(R.string.reminders_deleted));
                    else showSnackbar(mCoordinator, getString(R.string.reminder_deleted));
                    return true;
                default: return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mAdapter.setCabMode(mRecycler, false);
            getCabHost().onCabStopped();
        }
    };

    /**
     * If the default notification channel (Android O+) is blocked, the
     * foreground services may have troubles. Display a warning and a
     * button to the Notification settings page.
     */
    private void checkIfNotificationIsBlocked() {
        if(getContext() == null) return;

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager notifMan = getActivity().getSystemService(NotificationManager.class);
            NotificationChannel channel = notifMan.getNotificationChannel(getString(R.string.notification_channel_id));
            if(channel.getImportance() == NotificationManager.IMPORTANCE_NONE) {
                showSnackbarWithAction(mCoordinator,
                        getString(R.string.notification_channel_disabled),
                        getString(R.string.unblock),
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent i = new Intent(Settings.ACTION_APP_NOTIFICATION_SETTINGS);
                                i.putExtra(Settings.EXTRA_APP_PACKAGE, getString(R.string.app_package));
                                startActivity(i);
                            }
                        });
            }
        }
    }
}
