package batrand.android.popupreminder.views;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import batrand.android.popupreminder.R;
import batrand.android.popupreminder.services.formatter.IFormatService;
import batrand.android.popupreminder.services.reminder.IReminderService;
import batrand.android.popupreminder.views.adapters.NotificationsAdapter;
import batrand.android.popupreminder.views.adapters.ReloadDoneCallback;
import batrand.android.popupreminder.views.bases.BaseFragment;
import batrand.android.popupreminder.views.callbacks.IMainCabActivity;
import batrand.android.popupreminder.views.callbacks.IOnRecyclerItemLongClick;
import batrand.android.popupreminder.views.components.EmptyableRecyclerView;

import static batrand.android.popupreminder.services.injection.SingletonsInjector.getInjector;

public class NotificationsFragment extends BaseFragment {

    @Override protected int getLayoutId() { return R.layout.fragment_notifications; }

    public NotificationsFragment() {} // Required empty public constructor

    public static NotificationsFragment newInstance(IMainCabActivity cabHost) {
        NotificationsFragment fragment = new NotificationsFragment();
        fragment.setCabHost(cabHost);
        return fragment;
    }

    @Inject IReminderService mReminderService;
    @Inject IFormatService mFormatter;

    private CoordinatorLayout mCoordinator;
    private EmptyableRecyclerView mRecycler;
    private NotificationsAdapter mAdapter;
    private SwipeRefreshLayout mRefresh;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutId(), container, false);
        getInjector().inject(this);

        mCoordinator = view.findViewById(R.id.notifications_coordinator);

        mRecycler = view.findViewById(R.id.notifications_recycler);
        mAdapter = new NotificationsAdapter(getActivity(), mItemLongClickHandler);
        mRecycler.setAdapter(mAdapter);
        mRecycler.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        mRecycler.setEmptyView(view.findViewById(R.id.notifications_recycler_empty));

        mRefresh = view.findViewById(R.id.notifications_recycler_swipe);
        mRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mAdapter.reload(new ReloadDoneCallback() {
                   @Override
                    public void onReloadDone() {
                       mRefresh.setRefreshing(false);
                   }
                });
            }
        });

        return view;
    }

    private BroadcastReceiver mReloadReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(!intent.getAction().equals(mReminderService.getConfigs().getChangeAvailableBroadcastString()))
                return;
            mAdapter.reload(null);
        }
    };

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mReloadReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getActivity())
                .registerReceiver(mReloadReceiver,
                        new IntentFilter(mReminderService.getConfigs().getChangeAvailableBroadcastString())
                );
    }

    private IOnRecyclerItemLongClick mItemLongClickHandler = new IOnRecyclerItemLongClick() {
        @Override
        public void onItemLongClick(int itemPosition) {
            getCabHost().startCab(mCabCallback);

            // Start CAB visuals in adapter
            mAdapter.setCabMode(mRecycler, true);
        }
    };

    private ActionMode.Callback mCabCallback = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.notifications_menu, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) { return false; }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            int selectionCount;
            switch(item.getItemId()) {
                case R.id.notifications_menu_selectall:
                    mAdapter.selectAll(mRecycler);
                    return true;
                case R.id.notifications_menu_mark_seen:
                    selectionCount = mAdapter.selectionCount(mRecycler);
                    if(selectionCount < 1) return true;

                    mAdapter.markSeenSelected(mRecycler);
                    mAdapter.reload(null);
                    mode.finish();

                    if(selectionCount > 1) showSnackbar(mCoordinator, getString(R.string.notifications_marked_seen));
                    else showSnackbar(mCoordinator, getString(R.string.notification_marked_seen));
                    return true;
                case R.id.notifications_menu_delete:
                    selectionCount = mAdapter.selectionCount(mRecycler);
                    if(selectionCount < 1) return true;

                    mAdapter.deleteSelected(mRecycler);
                    mAdapter.reload(null);
                    mode.finish();

                    if(selectionCount > 1) showSnackbar(mCoordinator, getString(R.string.notifications_deleted));
                    else showSnackbar(mCoordinator, getString(R.string.notification_deleted));
                    return true;
                default: return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mAdapter.setCabMode(mRecycler, false);
            getCabHost().onCabStopped();
        }
    };
}
