package batrand.android.popupreminder.views.callbacks;

import android.support.v7.view.ActionMode;

/**
 * Activity that hosts the contextual action bar
 */

public interface IMainCabActivity {
    void startCab(ActionMode.Callback callback);
    void onCabStopped();
}
