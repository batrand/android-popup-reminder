package batrand.android.popupreminder.views;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.mikepenz.aboutlibraries.Libs;
import com.mikepenz.aboutlibraries.LibsBuilder;

import batrand.android.popupreminder.R;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        // Setting version info
        TextView appVersion = (TextView) findViewById(R.id.app_version_textview);
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String versionString = getString(R.string.app_version_formattable, pInfo.versionName, Integer.toString(pInfo.versionCode));
            appVersion.setText(versionString);
        } catch(PackageManager.NameNotFoundException e) {
            appVersion.setVisibility(View.GONE);
        }

        // Click listeners
        findViewById(R.id.about_button_libs).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new LibsBuilder()
                        .withActivityStyle(Libs.ActivityStyle.LIGHT_DARK_TOOLBAR)
                        .withActivityTitle(getString(R.string.title_about_libs))
                        .withAutoDetect(true)
                        .withLicenseShown(true)
                        .withLicenseDialog(true)
                        .withLibraries("Dagger","MaterialDTPicker", "MaterialSeekbarPref")
                        .start(AboutActivity.this);
            }
        });
        findViewById(R.id.about_button_audio).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AboutActivity.this, AboutSoundsActivity.class));
            }
        });
        findViewById(R.id.about_button_logs).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AboutActivity.this, LogActivity.class));
            }
        });
        findViewById(R.id.about_button_sourcelink).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.app_sourcelink)));
                startActivity(intent);
            }
        });
    }
}
